const request = require('request');
const CryptoJS = require("crypto-js");

var unlock = function(){
  // Get a token
  var token_url = "https://clp-accept-identityserver.my-clay.com/connect/token"
  var token_headers = { 
    "Authorization":"Basic OTJmNjcyNDEtOGM2MC0xMWU5LWFkNTctMDAwZDNhNDZhODgwOmFiYTc2MTYxLTRmM2QtNDdmNy1hMTU3LTkyN2EwMTJjNzRlNQ=="
  }
  var token_request = {
    url: token_url,
    headers: token_headers,
    form: {
      'grant_type':'password',
      'scope':'user_api.full_access',
      'username':'kateryna+@my-clay.com',
      'password':'HackCoworking123!'
    }
  }

  request.post(token_request, (err, res, body) => {
    if (err) { 
      return console.log(err);
    }

    var token = JSON.parse(body).access_token
    console.log("Token collected: " + token.slice(0,10) + "...")
    unlock_with_token(token)
  });
}

var unlock_with_token = function(token){
  // Get a One Time Pass
  console.log('Unlocked!')
  var otp = generate_otp()

  // Submit the unlock code
  var unlock_url = "https://clp-accept-user.my-clay.com/v1.1/sites/84f21b81-ec6f-405f-8d72-6782a4432013/locks/151beef4-b2ae-40b0-8ddb-a052acd0c681/locking"
  var unlock_headers = { 
    "Authorization": "Bearer " + token
  }
  var unlock_request = {
    url: unlock_url,
    headers: unlock_headers,
    json: {
      "locked_state": "unlocked",
      "otp": otp
    }
  }

  console.log('try to unlock')
  request.patch(unlock_request, (err, res, body) => {
    if (err) { 
      return console.log(err);
    }
    console.log('Unlocked!')
  });
}

var generate_otp = function(){
  var d = new Date();
  var YYYY = d.getUTCFullYear();
  var mm = ('0' + (d.getUTCMonth() + 1)).slice(-2);
  var DD = ('0' + d.getUTCDate()).slice(-2);
  var HH = ('0' + d.getUTCHours()).slice(-2);
  var mmm = ('0' + d.getUTCMinutes()).slice(-2);
  var ss = ('0' + d.getUTCSeconds()).slice(-2);

  var datetime = YYYY+mm+DD+HH+mmm+ss;

  var secret = "621307FFD49D448C"
  var pin = "2019";

  var otp = CryptoJS.MD5(datetime+secret+pin).toString().slice(0,5);
  console.log("Token collected: " + otp)
  return otp;
}

module.exports = {
  unlock
}
