const functions = require('firebase-functions');
const admin = require('firebase-admin');
const fs = require('fs');
const twilio = require('./twilio');
var Cookies = require('cookies');

const verbs = {
  UNLOCK: 'unlock',
  RENT: 'rent',
  DELIVER: 'deliver'
}

var serviceAccount = require("./credentials.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://hcw-2019.firebaseio.com"
});
const db = admin.database();

const engine = require('./engine');

exports.sms = functions.https.onRequest((request, response) => {
  var mobile = request.body.From;
  var message = request.body.Body.toLowerCase();

  var verb = verbs.RENT;
  if (message.includes("unlock")) { verb = verbs.UNLOCK; }
  if (message.includes("deliver")) { verb = verbs.DELIVER; }

  var ref = db.ref(`usage/${mobile}`);
  ref.once("value", function(snapshot) {
    var usage = snapshot.val();
    if (usage == null) {
      // a current usage was not found, so can only start rental or delivery
      switch(verb) {
        case verbs.RENT:
          engine.startRental(mobile);
          break;
        case verbs.DELIVER:
          let customerRef = "chris27"; // TODO
          engine.startDelivery(customerRef, mobile);
          break;
      }
    } else {
      // a current usage is found, so we are ending rental or delivery
      if (verb == verbs.UNLOCK) {
        switch(usage.state) {
          case "renting":
            engine.endRental(mobile);
            break;
          case "waiting":
            engine.endDelivery(mobile);
            break;
        }
      } else {
        response.writeHead(200);
        response.end("Incorrect verb for rental state");
      }
    }

    // completed incoming SMS
    response.writeHead(200);
    response.end();

  }, function (errorObject) {
    console.log("The read failed: " + errorObject.code);

    response.writeHead(400);
    response.end();
  });
});

exports.signup = functions.https.onRequest((request, response) => {
  render(response, './templates/signup.html');
});

exports.register = functions.https.onRequest((request, response) => {
  let name = request.body.name;
  let mobile = request.body.mobile;
  console.log("mobile: " + mobile);
  let firstName = name.split(' ')[0].toLowerCase();
  let random = Math.floor(Math.random() * 99) + 1;
  let reference = `${firstName}${random}`;

  var ref = db.ref(`customer/${reference}`);

  ref.set({
    name: name,
    mobile: mobile,
    reference: reference,
    created: new Date().toISOString()
  });

  engine.userRegistered(mobile, name);

  var cookies = new Cookies(request, response);
  cookies.set('CustomerRef', reference);

  response.writeHead(302, { Location: 'begin' });
  response.end();
});

exports.begin = functions.https.onRequest((request, response) => {
  var cookies = new Cookies(request, response);
  let customerRef = cookies.get('CustomerRef');

  if (customerRef) {
    render(response, './templates/begin.html');
  } else {
    response.writeHead(302, { Location: 'signup' });
    response.end();
  }
});

exports.start_rental = functions.https.onRequest((request, response) => {
  var cookies = new Cookies(request, response);
  let customerRef = cookies.get('CustomerRef');

  if (customerRef) {
    customerDbRef(customerRef).once("value", function(snapshot) {
      var customer = snapshot.val();
      engine.startRental(customer.mobile);
    });

    render(response, './templates/rental_started.html');
  } else {
    response.writeHead(302, { Location: 'signup' });
    response.end();
  }
});

exports.end_rental = functions.https.onRequest((request, response) => {
  var cookies = new Cookies(request, response);
  let customerRef = cookies.get('CustomerRef');

  if (customerRef) {
    customerDbRef(customerRef).once("value", function(snapshot) {
      var customer = snapshot.val();
      engine.endRental(customer.mobile);
    });
  }

  response.writeHead(302, { Location: 'begin' });
  response.end();
});

exports.reset = functions.https.onRequest((request, response) => {
  var cookies = new Cookies(request, response);
  cookies.set('CustomerRef');
  response.writeHead(302, { Location: 'begin' });
  response.end();
});


// ------ PRIVATE FUNCTIONS -------

function customerDbRef(customerRef) {
  return db.ref(`customer/${customerRef}`);
}

function render(response, filename) {
  response.writeHead(200, {'Content-Type': 'text/html'});
    fs.readFile(filename, null, function(error, data) {
      if (error) {
        response.writeHead(404);
        response.write('File not found!');
      } else {
        response.write(data);
      }
      response.end();
    });
}
