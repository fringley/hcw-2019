// Co-Locker
// Engine.js

const functions = require('firebase-functions');
const admin = require('firebase-admin');
const twilio = require('./twilio');
const salto = require('./salto');
const db = admin.database();

const LOCKER = "A1";
const LOCATION = "HUBHUB Farringdon";

let customer = {}; // stops parsing issues below
const START_RENTAL_SMS = `Locker ${LOCKER} at ${LOCATION} has been opened 🔓 Text 'UNLOCK' to this number to unlock and end rental.`;
const END_RENTAL_SMS = `Locker ${LOCKER} at ${LOCATION} has been opened and your rental has now ended 🔚 Thanks for using Co-Locker! 👋`;
const START_DELIVERY_CUSTOMER_SMS = `Your delivery has been dropped off! 📦 Open locker ${LOCKER} at ${LOCATION} by texting 'UNLOCK' to this number to pick up your item and end the service.`;
const START_DELIVERY_DRIVER_SMS = `Locker ${LOCKER} at ${LOCATION} has been opened. Delivery for ${customer.reference} (${customer.name}) has been registered.`;
const END_DELIVERY_SMS = `Locker ${LOCKER} at ${LOCATION} is now open 🔓 and your delivery service has been completed. Thanks for using Co-Locker! 👋`;


function createUsage(mobileNumber, state) {
  var ref = db.ref("usage/" + mobileNumber);

  ref.set({
    mobile: mobileNumber,
    state: state
  });
}

function deleteUsage(mobileNumber) {
  var ref = db.ref("usage/" + mobileNumber);
  ref.remove();
}

function sendSMS(mobile, message) {
  twilio.sendText(mobile, message);
}

function userRegistered(mobile, name) {
  var message = `Hey ${name} 👋 Thanks for registering with Co-Locker! 🔑`;
  twilio.sendText(mobile, message);
}

function startRental(mobileNumber) {
  console.log("start rental for: " + mobileNumber);

  salto.unlock();

  createUsage(mobileNumber, "renting");

  sendSMS(mobileNumber, START_RENTAL_SMS);
}

function endRental(mobileNumber) {
  console.log("end rental for: " + mobileNumber);

  salto.unlock();

  deleteUsage(mobileNumber);

  sendSMS(mobileNumber, END_RENTAL_SMS);
}

function startDelivery(customerRef, deliveryMobile) {
  console.log("start delivery for customer ref: " + customerRef);

  var ref = db.ref("customer/" + customerRef);
  ref.once("value", function(snapshot) {
    var customer = snapshot.val();

    salto.unlock();

    createUsage(customer.mobile, "waiting");

    sendSMS(deliveryMobile, START_DELIVERY_DRIVER_SMS);
    sendSMS(customer.mobile, START_DELIVERY_CUSTOMER_SMS);

  }, function (errorObject) {
    console.log("The read failed: " + errorObject.code);

    response.writeHead(400);
    response.end();
  });
}

function endDelivery(mobileNumber) {
  console.log("end delivery for: " + mobileNumber);

  salto.unlock();

  deleteUsage(mobileNumber);

  sendSMS(mobileNumber, END_DELIVERY_SMS);
}

module.exports = {
  userRegistered,
  startRental,
  endRental,
  startDelivery,
  endDelivery
}
