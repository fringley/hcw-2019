const twilioNumber = '+447479271278';
const accountSid = 'AC8212633e4a1be5f68740fb76131e909d';
const authToken = '4e2b2290595cc77008a381bbbe40fd78';

const twilio = require('twilio');
const client = new twilio(accountSid, authToken);

function sendText(phoneNumber, message) {
  console.log("Sending [" + message + "] to [" + phoneNumber + "]");

  if ( !validE164(phoneNumber) ) {
    throw new Error('number must be E164 format!')
  }

  const textContent = {
    body: message,
    to: phoneNumber,
    from: twilioNumber
  }

  client.messages.create(textContent)
    .then((message) => console.log("Sent"))
}

function respondToText(request, response, message) {
  const twiml = new twilio.twiml.MessagingResponse();

  twiml.message(message);

  response.writeHead(200, {'Content-Type': 'text/xml'});
  response.end(twiml.toString());
};

// Validate E164 format
function validE164(num) {
  return /^\+?[1-9]\d{1,14}$/.test(num)
}

module.exports = {
  sendText,
  respondToText
}
